# viktor

Platform that allows speakers, planners, and staff manage events at Linux Open Source Conferences and similar events. Codenamed 
"viktor" after the architect protagonist of the Russian sci-fi film "Koma."

## Getting started

Get started by joining the matrix channel https://matrix.to/#/!UBXSLSfMuEfJBiQkFh:matrix.org?via=matrix.org to discuss
what the project. Whether you're a speaker, staff member, participant, or volunteer...what do you want out of the platform?

For developers and maintainers we'll discuss what frameworks and stack would work best for this project.

## Maintainers

- Jordan Hewitt &lt;jordan@damngood.tech&gt; Initial maintainer and project coordinator.

🦸‍‍♀️ More maintainers & contributors welcome! 😁

## Join the Chat!

Join https://matrix.to/#/!UBXSLSfMuEfJBiQkFh:matrix.org?via=matrix.org for development discussions
